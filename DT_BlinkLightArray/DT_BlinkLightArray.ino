/*
  DT_BlinkLightArray.ino

  Blink an array of LEDs in sequence, then in reverse, with a pause at each end.
  Based on the example from http://www.arduino.cc/en/Tutorial/Array

*/

int onMillis = 800;       // Blink ON time, millis.
int offMillis = 200;      // Time between blinks.
int ledPins[] = {         // An array of pin numbers to which LEDs *might* be attached. Disconnected pins = a gap in time :-)
  9, 9, 3, 4, 5         // GCDuino. The onboard LED is flashed twice.
//  13, 13, 3, 4            // Borderless Leonardo: The onboard LED is flashed twice.
};
int pinCount = 5;       // the number of pins (i.e. the length of the array)

void setup() {
  for (int thisPin = 0; thisPin < pinCount; thisPin++) pinMode(ledPins[thisPin], OUTPUT);
}

void loop() {
  for (int thisPin = 0; thisPin < pinCount; thisPin++) { // Blink through the LEDS
    digitalWrite(ledPins[thisPin], HIGH);
    delay(onMillis);
    digitalWrite(ledPins[thisPin], LOW);
    delay(offMillis);
  }
  delay(2* (onMillis + offMillis));
  for (int thisPin = pinCount - 1; thisPin >= 0; thisPin--) { // Reverse blink through the LEDs
    digitalWrite(ledPins[thisPin], HIGH);
    delay(onMillis);
    digitalWrite(ledPins[thisPin], LOW);
    delay(offMillis);
  }
  delay(2* (onMillis + offMillis));
}
