#include <TimerOne.h>
#include <SPI.h>
#include <DMD.h>
#include "DHT.h"
//#include "Arial14.h"
#include "Arial_black_16.h"
#include "SystemFont5x7.h"

/*
1. Use 'Arduino Diecimila or Duemilanove w/ ATmega168': recommended on the Seeduino website

2. Hit the reset button at the start of the upload, else you will just get...

Binary sketch size: 11,494 bytes (of a 14,336 byte maximum)
avrdude: stk500_recv(): programmer is not responding
avrdude: stk500_getsync() attempt 1 of 10: not in sync: resp=0x00
...etc
*/


/* const String textToScroll = "David Tangye :-) "; */
const String textToScroll = "The Last Great Frontier is in Your Mind, mate :-) ... ";
const int showTempMillis = 10000;
const int showHumidMillis = 3000;
const int scrollWaiter = 30;

/* change these values if you have more than one DMD connected */
const int DISPLAYS_ACROSS = 1;   //for single DMD display
const int DISPLAYS_DOWN = 1;     //for single DMD display

const int line2Y = 8;
const int lMargin = 2;

#define DHTPIN 2
#define DHTTYPE DHT22

const int digitYposn1 = 0+lMargin;
const int digitYposn2 = 7+lMargin;
const int dotPosn = 13+lMargin;
const int digitYposn3 = 15+lMargin;
const int suffixPosn = 22+lMargin;

float sensorFloat = 0.0;

int intTen = 0;
int intUnit = 0;
int intDecimal = 0;

int intWork = 0;
int tempInt = 0;

DMD dmd(DISPLAYS_ACROSS,DISPLAYS_DOWN);
DHT dht(DHTPIN, DHTTYPE);

/*
	Todo: Check whether this function is needed, or can the code be nested into the one place it is used - DT 26/07/2015 15:42:31 (AEST)
*/
void ScanDMD() {
  dmd.scanDisplayBySPI();
}


void setup(void) {

  dht.begin();

  Timer1.initialize(5000);           
  Timer1.attachInterrupt(ScanDMD);   
//  Serial.begin(9600);
}


void scrollText(String dispString) {
  dmd.clearScreen( true );
  dmd.selectFont( Arial_Black_16 );
  char newString[256];
  int sLength = dispString.length();
  dispString.toCharArray( newString, sLength+1 );
  dmd.drawMarquee(newString,sLength,( 32*DISPLAYS_ACROSS )-1 , 0 );
  long start=millis();
  long timer=start;
  long timer2=start;
  boolean ret=false;
  while(!ret) {
    if ( ( timer+scrollWaiter ) < millis() ) {
      ret=dmd.stepMarquee( -1 , 0 );
      timer=millis();
    }
  }
}

void loop(void) {
  scrollText(textToScroll);

  dmd.selectFont(System5x7);
  dmd.drawString(lMargin, 0, "Temp", 5, GRAPHICS_NORMAL);

  sensorFloat = dht.readTemperature();

  intWork = sensorFloat * 10;
  intTen = intWork / 100;
  char theChar = char(intTen) + 48;
  dmd.drawChar(digitYposn1,line2Y,theChar,GRAPHICS_NORMAL);

  tempInt = intTen * 100;
  intWork = intWork - tempInt;
  intUnit = intWork / 10;
  tempInt = intUnit * 10;
  intWork = intWork - tempInt;
  theChar = char(intUnit) + 48;
  dmd.drawChar(digitYposn2,line2Y,theChar,GRAPHICS_NORMAL);

  dmd.writePixel(dotPosn,14,GRAPHICS_NORMAL,1);

  intDecimal = intWork / 1;
  theChar = char(intDecimal) + 48;
  dmd.drawChar(digitYposn3,line2Y,theChar,GRAPHICS_NORMAL);

  dmd.drawChar(suffixPosn,line2Y,'c', GRAPHICS_NORMAL);
  delay(showTempMillis);


  dmd.writePixel(dotPosn,14,GRAPHICS_NORMAL,0);
  dmd.drawString(lMargin, 0, "Humid",5, GRAPHICS_NORMAL);

  sensorFloat = dht.readHumidity();

  dmd.drawChar(digitYposn1,line2Y,' ',GRAPHICS_NORMAL);
  intWork = sensorFloat * 10;
  intTen = intWork / 100;
  theChar = char(intTen) + 48;
  dmd.drawChar(digitYposn2,line2Y,theChar,GRAPHICS_NORMAL);
  
  tempInt = intTen * 100;
  intWork = intWork - tempInt;
  intUnit = intWork / 10;
  theChar = char(intUnit) + 48;
  dmd.drawChar(digitYposn3,line2Y,' ',GRAPHICS_NORMAL);
  dmd.drawChar(digitYposn3-1,line2Y,theChar,GRAPHICS_NORMAL);

/*
  dmd.writePixel(dotPosn,15,GRAPHICS_NORMAL,1);
  tempInt = intUnit * 10;
  intWork = intWork - tempInt;
  intDecimal = intWork / 1;
  theChar = char(intDecimal) + 48;
  dmd.drawChar(digitYposn3,line2Y,theChar,GRAPHICS_NORMAL);
*/

  dmd.drawChar(suffixPosn,line2Y,'%', GRAPHICS_NORMAL);
  delay(showHumidMillis);
}

