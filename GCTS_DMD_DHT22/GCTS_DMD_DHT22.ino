#include <TimerOne.h>
#include <SPI.h>
#include <DMD.h>
#include "DHT.h"
//#include "Arial14.h"
#include "Arial_black_16.h"
#include "SystemFont5x7.h"

const String textToScroll = "Gold Coast TechSpace ";
const int yPosn = 9;

/* change these values if you have more than one DMD connected */
const int DISPLAYS_ACROSS = 1;   //for single DMD display
const int DISPLAYS_DOWN = 1;     //for single DMD display

#define DHTPIN 2
#define DHTTYPE DHT22

const int showTempMillis = 5000;
const int showHumidMillis = 5000;

const int hundredPosn = 0;
const int tenPosn = 7;
const int unitPosn = 14;
const int suffixPosn = 21;

int hundred_t = 0;
int ten_t = 0;
int unit_t = 0;

int hundred_h = 0;
int ten_h = 0;
int unit_h = 0;

int temp = 0;
int humid = 0;

int tempInt = 0;
float tempFloat = 0.0;
char tempChar = '.';

DMD dmd(DISPLAYS_ACROSS,DISPLAYS_DOWN);
DHT dht(DHTPIN, DHTTYPE);


void ScanDMD() {
  dmd.scanDisplayBySPI();
}


void setup(void) {

  dht.begin();

  Timer1.initialize(5000);           
  Timer1.attachInterrupt(ScanDMD);   
//  Serial.begin(9600);
}


void drawText(String dispString) {
  dmd.clearScreen( true );
  dmd.selectFont( Arial_Black_16 );
  char newString[256];
  int sLength = dispString.length();
  dispString.toCharArray( newString, sLength+1 );
  dmd.drawMarquee(newString,sLength,( 32*DISPLAYS_ACROSS )-1 , 0 );
  long start=millis();
  long timer=start;
  long timer2=start;
  boolean ret=false;
  while(!ret) {
    if ( ( timer+20 ) < millis() ) {
      ret=dmd.stepMarquee( -1 , 0 );
      timer=millis();
    }
  }
}

void loop(void) {
  drawText(textToScroll);

  dmd.writePixel(13,15,GRAPHICS_NORMAL,1);

  tempFloat = dht.readTemperature();

  temp = tempFloat * 10;
  hundred_t = temp / 100;

  tempInt = hundred_t * 100;
  temp = temp - tempInt;
  ten_t = temp / 10;

  tempInt = ten_t * 10;
  temp = temp - tempInt;
  unit_t = temp / 1;

/*
  float tempFloat = dht.readHumidity();

  humid = tempFloat * 10;
  hundred_h = humid / 100;
  
  tempInt = hundred_h * 100;
  humid = humid - tempInt;
  ten_h = humid / 10;

  tempInt = ten_h * 10;
  humid = humid - tempInt;
  unit_h = humid / 1;
*/

//  Serial.println(hundred_t);

  dmd.selectFont(System5x7);
  dmd.drawString(0,0, "Temp", 5, GRAPHICS_NORMAL);

  char theChar = char(hundred_t) + 48;
  dmd.drawChar(hundredPosn,yPosn,theChar,GRAPHICS_NORMAL);

  theChar = char(ten_t) + 48;
  dmd.drawChar(tenPosn,yPosn,theChar,GRAPHICS_NORMAL);

  theChar = char(unit_t) + 48;
  dmd.drawChar(unitPosn,yPosn,theChar,GRAPHICS_NORMAL);
  
  dmd.drawChar(suffixPosn,yPosn,'c', GRAPHICS_NORMAL);
  delay(showTempMillis);

/*
  dmd.drawString( 0,0,"Humid",4, GRAPHICS_NORMAL);

  theChar = char(hundred_h);
  dmd.drawChar(hundredPosn,yPosn,theChar,GRAPHICS_NORMAL);
  theChar = char(ten_h);
  dmd.drawChar(tenPosn,yPosn,theChar,GRAPHICS_NORMAL);
  theChar = char(unit_h);
  dmd.drawChar(unitPosn,yPosn,theChar,GRAPHICS_NORMAL);

  dmd.drawChar(suffixPosn,yPosn,'%', GRAPHICS_NORMAL);
  delay(showHumidMillis);
 */
}

