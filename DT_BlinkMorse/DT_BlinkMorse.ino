/*
    Flash out morse code, and optionally write the charactersout to the serial monitor
    David Tangye <DavidTangye@gmail.com> - 26/03/2018
*/

void setup() {

// ========== Start of stuff to configure

const String msg = "\'The Last Great Frontier is in Your Mind, Mate!\' (David Tangye - circa 1990)"; // Todo: Make this dynamic: get it from the keyboard, or a file or something?
//const String msg = "Test.";
// Set the dot flash length in milliseconds. This determines the speed of output of the morse code. 300 is good for learning morse.
const int dotL = 300;

const int morseLedPin = 4; // Best to use a pin that is not adjacent to any TX pin if you have set writing to the serial monitor to ON.
const int wordLedPin = 13; // On while each word is being flashed out. Off between words.

const boolean toSerialMonitor = false;  // Set to false before running away from a serial device (eg your computer)

// ========== End of stuff to configure

const String morseChars = " abcdefghijklmnopqrstuvwxyz0123456789/!,;:.?$&()-=+'\"";
const String morseCode[] = {"/", ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..", "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", "-..-.", "-.-.--", "--..--", "-.-.-.", "---...", ".-.-.-", "..--..", ".....", ".-...", "-.--.", "-.--.-", "-....-", "-...-", ".-.-.", ".----.", ".-..-."};
const int dashL = dotL * 3;
const int gap = dotL;
const int letterGap = dotL * 3 - gap;
const int wordGap = dotL * 7 - letterGap;

const int msgLen = msg.length();

  while(true) {             // For repeated output
    int letterLedState = LOW;
    int wordLedState = LOW;
  
    pinMode(morseLedPin, OUTPUT);     
    pinMode(wordLedPin, OUTPUT);
    digitalWrite(morseLedPin,LOW);
    digitalWrite(wordLedPin,LOW);

    if (toSerialMonitor) {
      digitalWrite(wordLedPin,HIGH);
      Serial.begin(9600);
      while (!Serial) { ; }
      Serial.println("Morse for : [" + msg + "] (" + msgLen + " characters including any spaces) ...");
      digitalWrite(wordLedPin,LOW);
    }
  
    unsigned int i, j, p, millisOn;
    String s;
    unsigned char c;
    String morseOfChar = "";
  
      digitalWrite(wordLedPin,HIGH);
      for (i=0;i<msgLen;i++) {
      s = msg.substring(i,i+1);
      c = msg.charAt(i);
  
      if(isAlpha(c) && isUpperCase(c)) {
        s.toLowerCase();
        c = s.charAt(0);
      }
  
      p = morseChars.indexOf(s);
      if (p >=0) {  
        morseOfChar = morseCode[p];
        if (morseOfChar != "") {
              if (toSerialMonitor) Serial.println("\t" + s + " " + morseOfChar);
          for(j=0;j<morseOfChar.length();j++) { // Generate output for each morse character [.-]
            char morseChar = String(morseOfChar[j]).charAt(0);
            switch (morseChar) {
              case '.':
                millisOn = dotL;
                break;
              case '-':
                millisOn = dashL;
                break;
              case '/':
                millisOn = letterGap;
                digitalWrite(wordLedPin,LOW);
                break;
              default:
                millisOn = 0;   // Should not happen.
            }
          if (morseChar != '/') digitalWrite(morseLedPin,HIGH);
          delay(millisOn);
          digitalWrite(morseLedPin,LOW);
          delay(gap);
          }
          delay(wordGap);
          digitalWrite(wordLedPin,HIGH);
        }
      } else if (toSerialMonitor) Serial.println("\t" + s + ": invalid");
    }
    if (toSerialMonitor) Serial.println("\t(End of message)");
  }             // For repeated output
}

void loop() { }
