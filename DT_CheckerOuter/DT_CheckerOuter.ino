/*
    DT_CheckerOuter.ino: A checker-outer for your arduino, mate.
    DavidTangye@gmail.com - 23/03/2018

    What it does, mate:

    Runs some tests on an arduino:
    1. Loop and see how fast the device is.
    Some results:
    Leonardo type from Borderless Electronics:
       1 second, 33847.00 loops / sec
      10 seconds, 1445.00 loops / sec`
      20 seconds, 1485.90 loops / sec
    GCDuino:
       1 second, 21669.00 loops / sec
      10 seconds, 5988.00 loops / sec
      20 seconds, 2724.85 loops / sec
      30 seconds, 1628.63 loops / sec


    2. Write timer MARKs to the consol at nominated minute marks.
    Useful to see how much an arduino's timer drifts over time.
    (Keep track of the real time on a separate device.)
    Also blinks 2 LEDs, (based on DT_Blink2) at intervals set below.

    Some results:
    Leonardo type from Borderless Electronics 4 hour test 23/03/18: still accurate within the accuracy of the test (1 second).
 */


// ---- Configuration variables (Stuff to alter, mate) ----------

// LED flash and timer data
const unsigned int looptestSecs = 40;   //  Time in seconds to run the looptest, assuming the clock is reasonably accurate

const unsigned long fastLedOnMillis = 100; // Fast LED, light ON.
const float fastCycleSecs = 5;      // Cycle time for the fast LED
const int slowLedHalfCycle = 3;     // Number of fast cycles per slow LED change (half cycle)
const int MarkMins = 1;             // Number of minutes (60,000 millisecs) per print of a time mark to console

// LED pin assignments
 const int fastLedPin = 9;  // Onboard GCDuino, Leostick?
//const int fastLedPin = 13; // Onboard Goldilocks
const int slowLedPin = 3;


// ---- The Program, mate -----

void setup() {

// ---- Program variables (Stuff that should not need altering, mate.) ----------

boolean loopTested = false;
unsigned int looptestCounter;

int fastLedCycleCount, slowLedCycleCount, timerCount = 0;
int fastLedState, slowLedState;

unsigned long loopMillis, speedEndMillis;
unsigned long oldMillis = 0;

const int slowLedCycle = slowLedHalfCycle * 2;
const float slowCycleSecs = fastCycleSecs * slowLedCycle;
const unsigned long fastLedOffMillis = (1000 * fastCycleSecs) - fastLedOnMillis;
unsigned long fastTimerMillis, timerStartMillis, timerEndMillis;
String m0, m1, m2, m3, m4, m5, theMsg;

// Timer test stuff
  pinMode(fastLedPin, OUTPUT);
  pinMode(slowLedPin, OUTPUT);
  digitalWrite(slowLedPin, HIGH); // Lit until serial comms is established
  digitalWrite(fastLedPin, LOW);
  Serial.begin(9600);
  while (!Serial) {
    ;
  }
  digitalWrite(fastLedPin, HIGH); // Lit while the speed loop test is running
  digitalWrite(slowLedPin, LOW);

  m1 = "DT-CheckerOuter\n\nTesting speed, counting loops over ";
  m2 = String(looptestSecs);
  m3 = " seconds...";
  theMsg = m1 + m2 + m3;
  Serial.println(theMsg);

  speedEndMillis = (looptestSecs * 1000) + millis();

  for(looptestCounter = 0; true; ) {
  unsigned long loopMillis = millis();

    if (loopTested == false) {
      if(loopMillis < speedEndMillis) {
        looptestCounter = looptestCounter + 1;
      } else {
        m1 = " - Completed ";
        m2 = String(looptestCounter);
        m3 = " loops in ";
        m4 = String(looptestSecs);
        m5 = " seconds.";
        theMsg = m1 + m2 + m3 + m4 + m5;
        Serial.println(theMsg);
        m1 = " - ";
        m2 = String(float(looptestCounter) / looptestSecs);
        m3 = " loops / sec.";
        theMsg = m1 + m2 + m3;
        Serial.println(theMsg);

        loopTested = true;

        m1 = "\nTimer Checker...\n - Fast LED (pin ";
        m2 = String(fastLedPin);
        m3 = "), cycle: ";
        m4 = String(fastCycleSecs);
        m5 = " seconds.";
        theMsg = m1 + m2 + m3 + m4 + m5;
        Serial.println(theMsg);
        m1 = " - Slow LED (pin ";
        m2 = String(slowLedPin);
        m3 = "), cycle: ";
        m4 = String(slowCycleSecs);
        m5 = " seconds.";
        theMsg = m1 + m2 + m3 + m4 + m5;
        Serial.println(theMsg);
        m1 = String(" - Testing clock over ");
        m2 = String(MarkMins);
        m3 = " minute intervals. (= ";
        float NumSlowCycles = 60 * MarkMins / slowCycleSecs;
        m4 = String(NumSlowCycles);
        m5 = String(" cycles of the long LED.)...\n");
        theMsg = m1 + m2 + m3 + m4 + m5;
        Serial.println(theMsg);

        fastLedState = LOW;
        slowLedState = HIGH;
        digitalWrite(fastLedPin, fastLedState);
        digitalWrite(slowLedPin, slowLedState);

        fastTimerMillis = fastLedOffMillis;
        timerStartMillis = millis();
        timerEndMillis = timerStartMillis + (MarkMins * 60000);
        m1 = "\t START:  ";
        m2 = String(timerStartMillis);
        m3 = " next -> ";
        m4 = String(timerEndMillis);
        m5 = " milliseconds...";
        theMsg = m1 + m2 + m3 + m4 + m5;
        Serial.println(theMsg);
      }
    } else {

      if(loopMillis - oldMillis >= fastTimerMillis) {
        if (fastLedState == LOW) {
          fastLedState = HIGH;
          fastTimerMillis = fastLedOnMillis;
          fastLedCycleCount = fastLedCycleCount + 1;
          if(fastLedCycleCount == slowLedHalfCycle) {
            if (slowLedState == LOW) slowLedState = HIGH;
            else slowLedState = LOW;
            digitalWrite(slowLedPin,slowLedState);
            fastLedCycleCount = 0;
          }
       }
        else {
          fastLedState = LOW;
          fastTimerMillis = fastLedOffMillis;
        }
        digitalWrite(fastLedPin, fastLedState);
        oldMillis = loopMillis;
      }
      if(loopMillis >= timerEndMillis) {
        timerCount = timerCount + 1;
        m0 = String(timerCount);
        m1 = "\t[MARK]: ";
        m2 = String(timerEndMillis);
        timerEndMillis = loopMillis + (MarkMins * 60000);
        m3 = " next -> ";
        m4 = String(timerEndMillis);
        m5 = " milliseconds...";
        theMsg = m0 + m1 + m2 + m3 + m4 + m5;
        Serial.println(theMsg);
      }
    }
  }
}

void loop() {
  Serial.println("WTF!");
  }
